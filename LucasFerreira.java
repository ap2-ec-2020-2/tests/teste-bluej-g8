public class LucasFerreira {
    String nome = "Lucas";
    String sobrenome = "Ferreira";
    int semestre = 2;
    
    public void nomeCompleto() {
        System.out.printf("Nome completo: %s %s%n", this.nome, this.sobrenome);
    }
    
    public int getSemestre() {
        return semestre;    
    }
    
}