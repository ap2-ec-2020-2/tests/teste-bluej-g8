public class EduardoCaixeta
{
    String nome = "Eduardo";
    String sobrenome = "Caixeta";
    int semestre = 3;
    
    public void nomeCompleto ()
    {
        System.out.printf("Nome completo: %s %s%n", this.nome, this.sobrenome);
    }
    
    public int getSemestre ()
    {
        return semestre;
    }
}